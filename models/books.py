from pymongo import MongoClient

def database:
    def __init__(self):
    try:
        self.nosql_db = MongoClient()
        self.db = self.nosql_db.perpustakaan
        mongo_col = self.db.books
        print("database connected")
    except Exception as e:
        print(e)
