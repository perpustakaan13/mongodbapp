from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import create_engine, Column, Integer, String, engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Boolean
from sqlalchemy.orm import relationship

Base = declarative_base()

class database:
    def __init__(self):
        engine = create_engine('mysql+mysqlconnector://root:Mysqlbase103@localhost:3306/todolist', echo = True)
        Session = sessionmaker(bind=engine)
        session = Session()

class Users(Base):
   __tablename__ = 'user'
   userid = Column(Integer, primary_key = True, nullable=False, autoincrement=True)
   username = Column(String, nullable=False)
   is_admin = Column(Boolean, nullable=False)

class Todolists(Base):
   __tablename__ = 'todolist'
   todoid = Column(Integer, primary_key = True, nullable=False, autoincrement=True)
   text = Column(String)
   userid = Column(Integer, ForeignKey("user.userid"))
   user = relationship("Users", back_populates="todolist")

Users.todolist = relationship("Todolists", order_by = Todolists.todoid, back_populates = "user")
Base.metadata.create_all(engine)